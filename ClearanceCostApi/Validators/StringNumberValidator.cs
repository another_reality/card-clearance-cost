using System;
using System.ComponentModel.DataAnnotations;
using ClearanceCostApi.Extensions;

namespace ClearanceCostApi.Validators
{
    [AttributeUsage(AttributeTargets.Property)]
    public class StringNumberValidator : ValidationAttribute
    {
        /// <summary>
        /// Json validation property, check if a string is a valid number from the request model
        /// Ref. CardClearanceRequest
        /// </summary>
        /// <param name="propertyValue">the incoming string</param>
        /// <returns></returns>
        public override bool IsValid(object propertyValue)
        {
            var stringValue = propertyValue as string;
            return stringValue.IsNumeric();
        }
    }
}
