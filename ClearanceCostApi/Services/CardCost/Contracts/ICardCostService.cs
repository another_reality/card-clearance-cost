using System.Threading.Tasks;
using ClearanceCostApi.Clients.BinListClient.Models;
using ClearanceCostApi.Models.Responses;

namespace ClearanceCostApi.Services.CardCost.Contracts
{
    public interface ICardCostService
    {
        Task<CardClearanceResponse> GetCost(CardInfoResponse cardInfo);
    }
}