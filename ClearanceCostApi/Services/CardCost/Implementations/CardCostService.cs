using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClearanceCostApi.Clients.BinListClient.Models;
using ClearanceCostApi.Constant;
using ClearanceCostApi.Models.Responses;
using ClearanceCostApi.Services.CardCost.Contracts;
using ClearanceCostApi.Services.Store.Contracts;
using Microsoft.Extensions.Configuration;

namespace ClearanceCostApi.Services.CardCost.Implementations
{  
    public class CardCostService : ICardCostService
    {
        private readonly IStoreService _storeService;
        private readonly IConfiguration _configuration;

        public CardCostService(IConfiguration configuration, IStoreService storeService)
        {
            _configuration = configuration;
            _storeService = storeService;
        }

        /// <summary>
        /// Calculate card clearance cost based on the country.
        /// Data for calculation are stored at .\StoreFiles\CardCost\CostsByCountry.json
        /// </summary>
        /// <param name="cardInfo">the model as returned from BinList client</param>
        /// <returns>Cost by country</returns>
        public async Task<CardClearanceResponse> GetCost(CardInfoResponse cardInfo) 
        { 
            var cardCosts = await _storeService.Get<List<CardClearanceResponse>>(
                _configuration.GetSection("Store:CardCostFolder").Value, "CostsByCountry");
            
            if (cardCosts.Exists(c => c.Country.ToLower().Equals(cardInfo.Country.Alpha2.ToLower())))
            {
                return cardCosts.FirstOrDefault(c => c.Country.ToLower().Equals(cardInfo.Country.Alpha2.ToLower()));
            }

            var otherCountry = cardCosts.FirstOrDefault(c => c.Country.ToLower().Equals(
                Project.OtherCountries.ToLower()));
            otherCountry.Country = cardInfo.Country.Alpha2;

            return cardCosts.FirstOrDefault(c => c.Country.ToLower().Equals(cardInfo.Country.Alpha2.ToLower()));
        }
    }
}