using System.Threading.Tasks;

namespace ClearanceCostApi.Services.Store.Contracts
{
    public interface IStoreService
    {
        Task Write<T>(string storeSpace, string identifier, T dataToWrite);
        Task<T> Get<T>(string storeSpace, string identifier);
        Task Remove(string storeSpace, string identifier);
    }
}