using System.IO;
using System.Threading.Tasks;
using ClearanceCostApi.Services.Store.Contracts;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace ClearanceCostApi.Services.Store.Implementations
{
    public class StoreService : IStoreService
    {
        private readonly string _filePath;

        /// <summary>
        /// Construct the filepath on where the files are stored.
        /// .\StoreFiles\CardInfo\*
        /// </summary>
        /// <param name="configuration"></param>
        public StoreService(IConfiguration configuration)
        {
            _filePath = string.Concat(
                System.Environment.CurrentDirectory, Path.DirectorySeparatorChar,
                configuration.GetSection("Store:BaseFolder").Value,
                Path.DirectorySeparatorChar, "{0}", Path.DirectorySeparatorChar, "{1}.json");
        }

        /// <summary>
        /// Write files to the store directory
        /// </summary>
        /// <param name="storeSpace"> Store space inside the StoreFiles directory</param>
        /// <param name="identifier"> The final json identifier to save, aka the file name. This is the 8 chars of the card</param>
        /// <param name="dataToWrite"> The data to be write on the file</param>
        /// <typeparam name="T"></typeparam>
        public async Task Write<T>(string storeSpace, string identifier, T dataToWrite)
        {
            var jsonToWrite = JsonConvert.SerializeObject(dataToWrite);
            using (TextWriter writeFile = new StreamWriter(string.Format(_filePath, storeSpace, identifier)))
            {
                await writeFile.WriteLineAsync(jsonToWrite);
            }
        }
        
        /// <summary>
        /// Remove a file from the store directory
        /// </summary>
        /// <param name="storeSpace"> Store space inside the StoreFiles directory</param>
        /// <param name="identifier"> The final json identifier to save, aka the file name. This is the 8 chars of the card</param>
        /// <returns></returns>
        public async Task Remove(string storeSpace, string identifier)
        {
            File.Delete(string.Format(_filePath, storeSpace, identifier));
        }

        /// <summary>
        /// Get a file from the store directory deserialized to the given object
        /// </summary>
        /// <param name="storeSpace"> Store space inside the StoreFiles directory</param>
        /// <param name="identifier"> The final json identifier to save, aka the file name. This is the 8 chars of the card</param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public async Task<T> Get<T>(string storeSpace, string identifier)
        {
            using (var readFile = new StreamReader(string.Format(_filePath, storeSpace, identifier)))
            {
                var jsonToRead = await readFile.ReadToEndAsync();
                return JsonConvert.DeserializeObject<T>(jsonToRead);
            }
        }
    }
}