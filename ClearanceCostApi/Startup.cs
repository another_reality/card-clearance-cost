using System.IO;
using System.Reflection;
using ClearanceCostApi.Clients.BinListClient.Client.Contracts;
using ClearanceCostApi.Clients.BinListClient.Client.Implementations;
using ClearanceCostApi.Clients.BinListClient.Services.Contracts;
using ClearanceCostApi.Clients.BinListClient.Services.Implementations;
using ClearanceCostApi.Services.CardCost.Contracts;
using ClearanceCostApi.Services.CardCost.Implementations;
using ClearanceCostApi.Services.Store.Contracts;
using ClearanceCostApi.Services.Store.Implementations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Serilog;
using Serilog.Events;
using Swashbuckle.AspNetCore.Swagger;

namespace ClearanceCostApi
{
    public class Startup
    {
        private const string CorsPolicyName = "AllowSpecificOrigins";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddCors(options =>
            {
                options.AddPolicy(CorsPolicyName,
                    builderCors => { builderCors.AllowAnyMethod().AllowAnyHeader().AllowAnyOrigin(); });
            });

            services.AddHttpClient();

            services.AddSingleton(Configuration);

            services.AddMvc(options =>
                {
                    options.RespectBrowserAcceptHeader = true;
                    options.ReturnHttpNotAcceptable = true;
                    options.OutputFormatters.Add(new XmlDataContractSerializerOutputFormatter());
                    options.OutputFormatters.Add(new XmlSerializerOutputFormatter());
                    options.InputFormatters.Add(new XmlSerializerInputFormatter(options));
                    options.OutputFormatters.RemoveType<StringOutputFormatter>();
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddXmlSerializerFormatters()
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                });
            
            var name = Assembly.GetExecutingAssembly().GetName();

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(Configuration)
                .MinimumLevel.Debug()
                .MinimumLevel.Override("clearanceCost", LogEventLevel.Information)
                .Enrich.FromLogContext()
                .Enrich.WithMachineName()
                .Enrich.WithProperty("Assembly", $"{name.Name}")
                .Enrich.WithProperty("Revision", $"{name.Version}")
                .WriteTo.Debug(
                    outputTemplate:
                    "[{Timestamp:HH:mm:ss} {Level:u3}] {Message:lj} {NewLine}{HttpContext} {NewLine}{Exception}")
                .WriteTo.RollingFile(string.Concat(@"logs", Path.DirectorySeparatorChar, "log-{Date}"),
                    LogEventLevel.Information, retainedFileCountLimit: 7)
                .CreateLogger();

            services.AddLogging(loggingBuilder =>
                loggingBuilder
                    .AddSerilog(dispose: true));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Card Clearance Cost API",
                    Description = "Card Clearance Cost API v1",
                    TermsOfService = "None",
                    Contact = new Contact
                    {
                        Name = "Margarit Koka", 
                        Email = "margariti@hotmail.com", 
                        Url = "www.linkedin.com/in/margarit-koka-09395539/"
                    }
                });
            });
            
            services.AddSingleton(Configuration);
            services.AddScoped<IBinListClient, BinListClient>();
            services.AddScoped<IBinListService, BinListService>();
            services.AddScoped<IStoreService, StoreService>();
            services.AddScoped<ICardCostService, CardCostService>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. See https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Card Clearance Cost API v1");
                c.RoutePrefix = string.Empty;
            });

            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
            );

            app.UseCookiePolicy();
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}