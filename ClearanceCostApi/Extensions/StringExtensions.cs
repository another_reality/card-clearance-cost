using System;
using System.Globalization;
using System.Linq;

namespace ClearanceCostApi.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Check if a string is number by parsing as double.
        /// Clear all whitespaces between tha string before check.
        /// Return true if not null, whitespace and not characters.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="style">Defaults to NumberStyles.Number</param>
        /// <param name="culture">Defaults to InvariantCulture</param>
        /// <returns>true : false</returns>
        public static bool IsNumeric(this string value, NumberStyles style = NumberStyles.Number,
            CultureInfo culture = null)
        {
            
            if (culture == null) culture = CultureInfo.InvariantCulture;
            return double.TryParse(value.ClearWhitespace(), style, culture, out _) && !string.IsNullOrWhiteSpace(value.ClearWhitespace());
        }
        
        /// <summary>
        /// Shorten card number to 8 numbers length if not null or smaller than 8 chars
        /// Clear all whitespaces between tha string before shorten.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="style"> Defaults to NumberStyles.Number </param>
        /// <exception cref="ArgumentException"> If string is null or smaller than 8 chars length</exception>
        /// <returns>A shorten version of the give string</returns>
        public static string ShortenCardNumber(this string value, NumberStyles style = NumberStyles.Number)
        {
            return !string.IsNullOrWhiteSpace(value) ? 
                string.Join(string.Empty, value.ClearWhitespace().Take(8)).Length < 8 ? 
                    throw new ArgumentException("Not Proper Credit Card Number") : 
                    string.Join(string.Empty, value.ClearWhitespace().Take(8)) :
                throw new ArgumentException("Not Proper Credit Card Number");
        }

        /// <summary>
        /// Clear all whitespaces from a given string
        /// </summary>
        /// <param name="value"></param>
        /// <returns>string without whitespaces</returns>
        public static string ClearWhitespace(this string value) 
        {
            return string.Join(string.Empty, value.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries));
        }
    }
}