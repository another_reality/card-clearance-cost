using System;

namespace ClearanceCostApi.Clients.BinListClient.Exceptions
{
    /// <summary>
    /// Throw this exception on 404 response code
    /// </summary>
    public class CardNotFoundException : Exception
    {
        public CardNotFoundException()
        {
        }
        
        public CardNotFoundException(string message) : base(message)
        { 
        }
    }
    
    /// <summary>
    /// Throw this exception on 429 response code
    /// </summary>
    public class ToManyRequestException : Exception
    {
        public ToManyRequestException()
        {
        }
        
        public ToManyRequestException(string message) : base(message)
        { 
        }
    }
}