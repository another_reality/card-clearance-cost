using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using ClearanceCostApi.Clients.BinListClient.Client.Contracts;
using ClearanceCostApi.Clients.BinListClient.Exceptions;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace ClearanceCostApi.Clients.BinListClient.Client.Implementations
{
    /// <summary>
    /// BinList Client implementation
    /// </summary>
    public class BinListClient : IBinListClient
    {
        private readonly HttpClient _httpClient;
        
        /// <summary>
        /// Constructor, create the httpClient with needed headers
        /// </summary>
        /// <param name="configuration"></param>
        public BinListClient(IConfiguration configuration)
        {
            _httpClient = new HttpClient
            {
                BaseAddress = new Uri(configuration.GetSection("BinListServiceUrl").Value)
            };
            _httpClient.DefaultRequestHeaders.Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _httpClient.DefaultRequestHeaders.Add("Accept-Version","3");
                
        }

        /// <summary>
        /// Make calls on BinList api
        /// </summary>
        /// <param name="cardNumber">card number that we wish to get info for</param>
        /// <typeparam name="T">Response model</typeparam>
        /// <returns></returns>
        /// <exception cref="CardNotFoundException"> Exception that return on 404</exception>
        /// <exception cref="ToManyRequestException"> Exception that return on 429</exception>
        /// <exception cref="WebException"> On all other errors return this exception</exception>
        public async Task<T> GetAsync<T>(string cardNumber)
        {
            var response = await _httpClient.GetAsync($"{cardNumber}");
            if (!response.IsSuccessStatusCode)
            {
                switch (response.StatusCode)
                {
                    case HttpStatusCode.NotFound:
                        throw new CardNotFoundException("The card could not found");
                    case HttpStatusCode.TooManyRequests:
                        throw new ToManyRequestException("To many request for one minute");
                    default: 
                        throw new WebException("A conflict happened with the httpClient");
                }
            }
            
            var result = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(result);
        }
    }
}