using System.Threading.Tasks;

namespace ClearanceCostApi.Clients.BinListClient.Client.Contracts
{
    public interface IBinListClient
    {
        Task<T> GetAsync<T>(string cardNumber);
    }
}