using System.Threading.Tasks;
using ClearanceCostApi.Clients.BinListClient.Client.Contracts;
using ClearanceCostApi.Clients.BinListClient.Models;
using ClearanceCostApi.Clients.BinListClient.Services.Contracts;

namespace ClearanceCostApi.Clients.BinListClient.Services.Implementations
{
    /// <summary>
    /// Service class for BinList client request
    /// </summary>
    public class BinListService : IBinListService
    {
        private readonly IBinListClient _binListClient;

        public BinListService(IBinListClient binListClient)
        {
            _binListClient = binListClient;
        }
        
        /// <summary>
        /// Get card info
        /// </summary>
        /// <param name="cardNumber">Card number that we wish to get info for</param>
        /// <returns>Card info response object from BinList</returns>
        public async Task<CardInfoResponse> GetCardInfo(string cardNumber)
        {
            return await _binListClient.GetAsync<CardInfoResponse>(cardNumber);
        }
    }
}