using System.Threading.Tasks;
using ClearanceCostApi.Clients.BinListClient.Models;

namespace ClearanceCostApi.Clients.BinListClient.Services.Contracts
{
    public interface IBinListService
    {
        Task<CardInfoResponse> GetCardInfo(string cardNumber);
    }
}