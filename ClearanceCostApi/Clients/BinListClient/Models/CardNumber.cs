namespace ClearanceCostApi.Clients.BinListClient.Models
{
    public class CardNumber
    {
        public int? Number { get; set; }
        public bool? Luhn { get; set; }
    }
}