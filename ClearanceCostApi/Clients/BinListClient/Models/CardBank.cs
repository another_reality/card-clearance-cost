namespace ClearanceCostApi.Clients.BinListClient.Models
{
    public class CardBank
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string Phone { get; set; }
        public string City { get; set; }
    }
}