namespace ClearanceCostApi.Clients.BinListClient.Models
{
    public class CardCountry
    {
        public string Numeric { get; set; }
        public string Alpha2 { get; set; }
        public string Name { get; set; }
        public string Emoji { get; set; }
        public string Currency { get; set; }
        public float? Latitude { get; set; }
        public float? Longitude { get; set; }
    }
}