namespace ClearanceCostApi.Models.Responses
{
    public class BaseResponse<T>
    {
        public T Message { get; set; }
    }
}