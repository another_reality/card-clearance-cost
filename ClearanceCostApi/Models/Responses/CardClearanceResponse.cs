namespace ClearanceCostApi.Models.Responses
{
    public class CardClearanceResponse
    {
        public string Country { get; set; }

        public float Cost { get; set; }
    }
}