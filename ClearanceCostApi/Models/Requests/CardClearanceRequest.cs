using System.ComponentModel.DataAnnotations;
using ClearanceCostApi.Validators;
using Newtonsoft.Json;

namespace ClearanceCostApi.Models.Requests
{
    public class CardClearanceRequest
    {
        [StringNumberValidator(ErrorMessage = "Only Numbers are allowed on card number")]
        [JsonProperty("card_number"), MaxLength(19), MinLength(8)]
        public string CardNumber { get; set; }
    }
}