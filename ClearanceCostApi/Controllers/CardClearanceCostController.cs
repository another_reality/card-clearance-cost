using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using ClearanceCostApi.Clients.BinListClient.Exceptions;
using ClearanceCostApi.Clients.BinListClient.Models;
using ClearanceCostApi.Clients.BinListClient.Services.Contracts;
using ClearanceCostApi.Extensions;
using ClearanceCostApi.Models.Requests;
using ClearanceCostApi.Models.Responses;
using ClearanceCostApi.Services.CardCost.Contracts;
using ClearanceCostApi.Services.Store.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Serilog;

namespace ClearanceCostApi.Controllers
{
    [ApiController]
    public class CardClearanceCostController : ControllerBase
    {
        private readonly IBinListService _binListService;
        private readonly IStoreService _storeService;
        private readonly IConfiguration _configuration;
        private readonly ICardCostService _cardCostService;

        public CardClearanceCostController(IBinListService binListService, IStoreService storeService,
            IConfiguration configuration, ICardCostService cardCostService)
        {
            _binListService = binListService;
            _storeService = storeService;
            _configuration = configuration;
            _cardCostService = cardCostService;
        }

        /// <summary>
        /// Provide a credit card and get the clearance cost based on your card country. Execution path:
        /// Short card number to 8 chars
        /// If card exists on store, calculate cost based on store file and return OK
        /// Request card information from BinList
        /// Save on storeFiles
        /// Calculate clearance cost and return OK
        /// </summary>
        /// <param name="cardClearanceRequest">Card number json</param>
        /// <remarks>Returns an object that contains the country and the cost</remarks>
        /// <response code="200">
        /// Successful response
        /// Example Body Value | Model
        /// {
        ///  "message": {
        ///    "country": "ZA",
        ///    "cost": 10
        ///   }
        /// }
        /// </response>
        /// <response code="400">
        /// Bad Request
        /// Example Value | Model
        /// </response>
        /// <response code="404">
        /// Card Not Found
        /// Example Value | Model
        /// </response>
        /// <response code="409">
        /// Conflict
        /// Example Value | Model
        /// </response>
        /// <response code="429">
        /// To Many Requests
        /// Example Value | Model
        /// </response>
        [Route("payment_cards_cost")]
        [HttpPost]
        public async Task<ActionResult<BaseResponse<CardClearanceResponse>>> PostCharge(
            CardClearanceRequest cardClearanceRequest)
        {
            var cardNumber = cardClearanceRequest.CardNumber.ShortenCardNumber();

            CardInfoResponse cardInfo;
            try
            {
                cardInfo = await _storeService.Get<CardInfoResponse>(
                    _configuration.GetSection("Store:CardInfoFolder").Value, cardNumber);
                var clearanceCost = await _cardCostService.GetCost(cardInfo);
                return Ok(new BaseResponse<CardClearanceResponse>
                {
                    Message = clearanceCost
                });
            }
            catch (FileNotFoundException ex)
            {
                Log.Information(string.Concat(cardNumber, ex.Message));
            }
            catch (DirectoryNotFoundException ex)
            {
                Log.Information(string.Concat(cardNumber, ex.Message));
            }
            catch (JsonSerializationException ex)
            {
                Log.Error("Could not resolve store card info");
                return Conflict(new BaseResponse<string>
                {
                    Message = "Could not resolve store card info"
                });
            }

            try
            {
                cardInfo = await _binListService.GetCardInfo(cardNumber);
            }
            catch (CardNotFoundException ex)
            {
                Log.Warning(ex.Message);
                return NotFound(new BaseResponse<string>
                {
                    Message = ex.Message
                });
            }
            catch (ToManyRequestException ex)
            {
                Log.Warning(ex.Message);
                return StatusCode((int) HttpStatusCode.TooManyRequests, ex.Message);
            }
            catch (WebException ex)
            {
                Log.Error(ex.Message);
                return Conflict(new BaseResponse<string>
                {
                    Message = ex.Message
                });
            }

            try
            {
                await _storeService.Write(_configuration.GetSection("Store:CardInfoFolder").Value,
                    cardNumber, cardInfo);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return Conflict(new BaseResponse<string>
                {
                    Message = "Something weired happened, shame"
                });
            }

            try
            {
                var clearanceCost = await _cardCostService.GetCost(cardInfo);
                return Ok(new BaseResponse<CardClearanceResponse>
                {
                    Message = clearanceCost
                });
            }
            catch (JsonSerializationException ex)
            {
                Log.Error(ex.Message);
                return Conflict(new BaseResponse<string>
                {
                    Message = "Could not calculate cost"
                });
            }
            catch (FileNotFoundException ex)
            {
                Log.Error(ex.Message);
                return Conflict(new BaseResponse<string>
                {
                    Message = "Could not calculate cost"
                });
            }
        }
    }
}