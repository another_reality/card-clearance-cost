using System;
using System.IO;
using System.Net.Http;
using System.Reflection;
using ClearanceCostApi;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;

namespace ClearanceCostTest
{
    public class TestContext
    {
        private TestServer _server;
        public HttpClient Client { get; private set; }

        public TestContext()
        {
            SetUpClient();
        }

        /// <summary>
        /// Setup client for integrationTests with testHost
        /// </summary>
        private void SetUpClient()
        {
            var projectDir = GetProjectPath("", typeof(Startup).GetTypeInfo().Assembly);
            _server = new TestServer(new WebHostBuilder()
                .UseEnvironment("Development")
                .UseContentRoot(projectDir)
                .UseConfiguration(new ConfigurationBuilder()
                    .SetBasePath(projectDir)
                    .AddJsonFile("appsettings.json")
                    .Build()
                ).UseContentRoot(projectDir)
                .UseStartup<Startup>());

            Client = _server.CreateClient();
        }
        
        /// <summary>
        /// Provide target project path
        /// </summary>
        /// <param name="projectRelativePath"></param>
        /// <param name="startupAssembly"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        private static string GetProjectPath(string projectRelativePath, Assembly startupAssembly)
        {
            var projectName = startupAssembly.GetName().Name;
            
            var applicationBasePath = AppContext.BaseDirectory;
    
            var directoryInfo = new DirectoryInfo(applicationBasePath);
            do
            {
                directoryInfo = directoryInfo.Parent;

                if (directoryInfo != null)
                {
                    var projectDirectoryInfo = new DirectoryInfo(Path.Combine(directoryInfo.FullName, projectRelativePath));
                    if (!projectDirectoryInfo.Exists) continue;
                    var projectFileInfo = new FileInfo(Path.Combine(projectDirectoryInfo.FullName, projectName, $"{projectName}.csproj"));
                    if (projectFileInfo.Exists)
                    {
                        return Path.Combine(projectDirectoryInfo.FullName, projectName);
                    }
                }
            }
            while (directoryInfo?.Parent != null);
            throw new Exception($"Could not locate application root project {applicationBasePath}.");
        }
    }
}