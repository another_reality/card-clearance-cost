using System;
using System.IO;
using System.Reflection;
using Xunit.Sdk;

namespace ClearanceCostTest.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AfterTestEmptyStore : BeforeAfterTestAttribute
    {
        /// <summary>
        /// After test is executed clear the test environment StoreFiles/CardInfo directory
        /// </summary>
        /// <param name="methodUnderTest"></param>
        public override void After(MethodInfo methodUnderTest)
        {
            var directory = new DirectoryInfo(string.Concat(
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
                Path.DirectorySeparatorChar,
                "StoreFiles", Path.DirectorySeparatorChar, "CardInfo"));
            EmptyStore(directory);
        }
        
        /// <summary>
        /// Helper method that clean the test directory
        /// </summary>
        /// <param name="store"></param>
        private static void EmptyStore(DirectoryInfo store)
        {
            foreach(var jsons in store.GetFiles()) jsons.Delete();
            foreach(var subStores in store.GetDirectories()) subStores.Delete(true);
        }
    }
}