using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ClearanceCostApi.Models.Requests;
using ClearanceCostApi.Models.Responses;
using ClearanceCostTest.Attributes;
using ClearanceCostTest.TheoriesData;
using Newtonsoft.Json;
using Xunit;

namespace ClearanceCostTest.IntegrationTest
{
    
    /// <summary>
    /// Integration Tests class
    /// </summary>
    public class CardClearanceCostTest
    {
        /// <summary>
        /// Should return bad request 400 as the requested credit card is bad formatted. Validator test
        /// </summary>
        /// <param name="cardNumber"></param>
        /// <returns></returns>
        [Theory]
        [InlineData("43534543AA")]
        [InlineData("OOO000112")]
        [InlineData("99999")] // less than eight 8
        [InlineData("999991111222333444564")] // more than nineteen 19
        public async Task Test_ShouldFail_CardClearanceCostBadCard(string cardNumber)
        {
            var context = new TestContext();

            using (var client = context.Client)
            {
                var cardClearanceRequest = new CardClearanceRequest
                {
                    CardNumber = cardNumber
                };
                
                HttpContent httpContent = new StringContent(
                    JsonConvert.SerializeObject(cardClearanceRequest), 
                    Encoding.UTF8, "application/json");
                
                var response = await client.PostAsync("/payment_cards_cost", httpContent);

                Assert.True(response.StatusCode == HttpStatusCode.BadRequest);
            }
        }
        
        /// <summary>
        /// Should return 404 as the provided credit card does not exists
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task Test_ShouldFail_CardNotFound()
        {
            var context = new TestContext();

            using (var client = context.Client)
            {
                var cardClearanceRequest = new CardClearanceRequest
                {
                    CardNumber = "000122332211"
                };
                
                HttpContent httpContent = new StringContent(
                    JsonConvert.SerializeObject(cardClearanceRequest), 
                    Encoding.UTF8, "application/json");
                
                var response = await client.PostAsync("/payment_cards_cost", httpContent);

                Assert.True(response.StatusCode == HttpStatusCode.NotFound);
            }
        }
        
        /// <summary>
        /// Should return 200 OK, this test make 3 requests with different country card and should expect proper costs by country
        /// US, GR, ZM. TheoriesData Ref. ClearanceCostByCountryData
        /// </summary>
        /// <param name="cardNumber"></param>
        /// <param name="expectedResponseBody"></param>
        /// <param name="responseStatus"></param>
        /// <returns></returns>
        [Theory]
        [ClassData(typeof(ClearanceCostByCountryData))]
        [AfterTestEmptyStore]
        public async Task Test_ShouldSuccess_CostClearanceByCountry(string cardNumber, CardClearanceResponse expectedResponseBody, HttpStatusCode responseStatus)
        {
            var context = new TestContext();

            using (var client = context.Client)
            {
                var cardClearanceRequest = new CardClearanceRequest
                {
                    CardNumber = cardNumber
                };
                
                HttpContent httpContent = new StringContent(
                    JsonConvert.SerializeObject(cardClearanceRequest), 
                    Encoding.UTF8, "application/json");
                
                var response = await client.PostAsync("/payment_cards_cost", httpContent);

                Assert.True(response.StatusCode == responseStatus);

                var responseBody = 
                    JsonConvert.DeserializeObject<BaseResponse<CardClearanceResponse>>(await response.Content.ReadAsStringAsync());
                Assert.True(expectedResponseBody.Cost == responseBody.Message.Cost);
                Assert.Equal(expectedResponseBody.Country,responseBody.Message.Country);
            }
        }
    
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="responseStatus"></param>
        /// <returns></returns>
        // TODO This test need a better look, sometimes BinList respond toManyRequest on 15calls/min sometimes on fewer. This doesnt seems stable
        // NOTE Your are welcome to try but expected to fail
//        [Theory]
//        [ClassData(typeof(ToManyRequestData))]
//        [AfterTestEmptyStore]
//        public async Task Test_ShouldFail_ToManyRequest(string value, HttpStatusCode responseCode)
//        {
//            var context = new TestContext();
//
//            using (var client = context.Client)
//            {
//                var cardClearanceRequest = new CardClearanceRequest
//                {
//                    CardNumber = value
//                };
//                
//                HttpContent httpContent = new StringContent(
//                    JsonConvert.SerializeObject(cardClearanceRequest), 
//                    Encoding.UTF8, "application/json");
//                
//                var response = await client.PostAsync("/payment_cards_cost", httpContent);
//
//                Assert.True(response.StatusCode == responseCode);
//            }
//        }
    }
}
