using System.IO;
using ClearanceCostApi.Clients.BinListClient.Client.Contracts;
using ClearanceCostApi.Clients.BinListClient.Client.Implementations;
using ClearanceCostApi.Clients.BinListClient.Services.Contracts;
using ClearanceCostApi.Clients.BinListClient.Services.Implementations;
using ClearanceCostApi.Services.CardCost.Contracts;
using ClearanceCostApi.Services.CardCost.Implementations;
using ClearanceCostApi.Services.Store.Contracts;
using ClearanceCostApi.Services.Store.Implementations;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ClearanceCostTest
{
    public class TestStartup
    {
        /// <summary>
        /// TestStartup container for needed unit test dependencies 
        /// </summary>
        public TestStartup()
        {
            var serviceCollection = new ServiceCollection();
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(
                    path: "appsettings.json",
                    optional: false,
                    reloadOnChange: true)
                .Build();
            serviceCollection.AddSingleton<IConfiguration>(configuration);
            serviceCollection.AddScoped<IBinListClient, BinListClient>();
            serviceCollection.AddScoped<IBinListService, BinListService>();
            serviceCollection.AddScoped<IStoreService, StoreService>();
            serviceCollection.AddScoped<ICardCostService, CardCostService>();

            ServiceProvider = serviceCollection.BuildServiceProvider();
        }

        public ServiceProvider ServiceProvider { get; private set; }
    }
}