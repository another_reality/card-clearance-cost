using ClearanceCostApi.Clients.BinListClient.Models;
using ClearanceCostApi.Models.Responses;
using Xunit;

namespace ClearanceCostTest.TheoriesData
{
    public class ClearanceCostByCountriesData : TheoryData <CardClearanceResponse, CardInfoResponse> 
    {
        public ClearanceCostByCountriesData()
        {
            Add(new CardClearanceResponse
            {
                Country = "US",
                Cost = 5
            }, new CardInfoResponse
            {
                Country = new CardCountry
                {
                    Alpha2 = "US"
                }
            });
            
            Add(new CardClearanceResponse
            {
                Country = "GR",
                Cost = 15
            }, new CardInfoResponse
            {
                Country = new CardCountry
                {
                    Alpha2 = "GR"
                }
            });
            
            Add(new CardClearanceResponse
            {
                Country = "ZM",
                Cost = 10
            }, new CardInfoResponse
            {
                Country = new CardCountry
                {
                    Alpha2 = "ZM"
                }
            });
        }
    }
}