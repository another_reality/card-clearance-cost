using System.Net;
using ClearanceCostApi.Models.Responses;
using Xunit;

namespace ClearanceCostTest.TheoriesData
{
    public class ClearanceCostByCountryData : TheoryData <string, CardClearanceResponse, HttpStatusCode> 
    {
        public ClearanceCostByCountryData()
        {
            Add("30353938", new CardClearanceResponse
            {
                Country = "US",
                Cost = 5
            }, HttpStatusCode.OK);
            
            Add("41022211", new CardClearanceResponse
            {
                Country = "GR",
                Cost = 15
            }, HttpStatusCode.OK);
            
            Add("40335091", new CardClearanceResponse
            {
                Country = "ZM",
                Cost = 10
            }, HttpStatusCode.OK);
        }
    }
}