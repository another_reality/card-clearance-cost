using System.Net;
using Xunit;

namespace ClearanceCostTest.TheoriesData
{
    public class ToManyRequestData : TheoryData<string, HttpStatusCode> 
    {
        public ToManyRequestData()
        {
            Add("43058970", HttpStatusCode.OK);
            Add("43058971", HttpStatusCode.OK);
            Add("43058972", HttpStatusCode.OK);
            Add("43058973", HttpStatusCode.OK);
            Add("43058974", HttpStatusCode.OK);
            Add("43058975", HttpStatusCode.OK);
            Add("43058976", HttpStatusCode.OK);
            Add("43058977", HttpStatusCode.OK);
            Add("43058978", HttpStatusCode.OK);
            Add("43058979", HttpStatusCode.OK);
            Add("43058989", HttpStatusCode.OK);
            Add("43058987", HttpStatusCode.OK);
            Add("43058980", HttpStatusCode.TooManyRequests);
        }
    }
}