using System;
using System.IO;
using ClearanceCostApi.Clients.BinListClient.Models;
using ClearanceCostApi.Extensions;
using ClearanceCostApi.Models.Responses;
using ClearanceCostApi.Services.CardCost.Contracts;
using ClearanceCostApi.Services.Store.Contracts;
using ClearanceCostTest.TheoriesData;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace ClearanceCostTest.UnitTests
{
    public class UnitTestServices : IClassFixture<TestStartup>
    {
        private readonly ServiceProvider _serviceProvide;
        private readonly IStoreService _storeService;
        private readonly IConfiguration _configuration;
        private readonly ICardCostService _cardCostService;
      
        public UnitTestServices(TestStartup setupFixture)
        {
            _serviceProvide = setupFixture.ServiceProvider;
            _storeService = _serviceProvide.GetService<IStoreService>();
            _configuration = _serviceProvide.GetService<IConfiguration>();
            _cardCostService = _serviceProvide.GetService<ICardCostService>();
        }

        private const string Identifier = "00 11 22 33 44 55";
        private const string CardInfoBrand = "BrandName";

        /// <summary>
        /// Test the store service, write on store, get the newly created store file, remove the store file
        /// and expect to throw an FileNotFoundException
        /// </summary>
        [Fact]
        public async void Test_Write_Read_Remove_Store()
        { 
            await _storeService.Write(
                _configuration.GetSection("Store:CardInfoFolder").Value, 
                Identifier.ShortenCardNumber(), new CardInfoResponse {Brand = CardInfoBrand});
            
            var cardInfo = await _storeService.Get<CardInfoResponse>(
                _configuration.GetSection("Store:CardInfoFolder").Value, Identifier.ShortenCardNumber());
            
            Assert.Equal(cardInfo.Brand,CardInfoBrand);
            
            await _storeService.Remove(
                _configuration.GetSection("Store:CardInfoFolder").Value, Identifier.ShortenCardNumber());

            await Assert.ThrowsAsync<FileNotFoundException>(() => _storeService.Get<CardInfoResponse>(
                _configuration.GetSection("Store:CardInfoFolder").Value, Identifier.ShortenCardNumber()));
        }
        
        /// <summary>
        /// Test the cost calculator service by providing 3 countries and expecting the proper costs
        /// </summary>
        /// <param name="expected"></param>
        /// <param name="provided"></param>
        [Theory]
        [ClassData(typeof(ClearanceCostByCountriesData))]
        public async void Test_Cost_Calculator(CardClearanceResponse expected, CardInfoResponse provided)
        { 
            var cardClearance = await _cardCostService.GetCost(provided);
            
            Assert.True(expected.Cost == cardClearance.Cost);
            Assert.Equal(expected.Country,cardClearance.Country);
        }
        
        /// <summary>
        /// Test string extension isNumeric, should fail bad card number
        /// </summary>
        /// <param name="value"></param>
        [Theory]
        [InlineData("43534543AA")]
        public void Test_ShouldFail_String_Extensions_IsNumeric(string value)
        { 
            Assert.False(value.IsNumeric());
        }
        
        /// <summary>
        /// Test string extension isNumeric, should pass
        /// Inline data only numbers
        /// </summary>
        /// <param name="value"></param>
        [Theory]
        [InlineData("43534543993")]
        [InlineData("43 53 45 43  99 3")]
        public void Test_ShouldPass_String_Extensions_IsNumeric(string value)
        { 
            Assert.True(value.IsNumeric());
        }
        
        /// <summary>
        /// Test card number shortener string extension. Should pass returned card number equals to 8 chars
        /// </summary>
        /// <param name="value"></param>
        [Theory]
        [InlineData("00 44 55 66 33 22 545454")]
        [InlineData("543554345543450909090")]
        public void Test_ShouldPass_String_Extensions_Shorten_Card_Number(string value)
        { 
            Assert.True(value.ShortenCardNumber().Length == 8);
        }
        
        /// <summary>
        /// Test card number shortener string extension. should throw ArgumentException as card number is smaller than 8 chars
        /// </summary>
        /// <param name="value"></param>
        [Theory]
        [InlineData("       ")]
        [InlineData("00 00 00")]
        public void Test_ShouldFail_String_Extensions_Shorten_Card_Number(string value)
        { 
            Assert.Throws<ArgumentException>(() => value.ShortenCardNumber());
        }
    }
}