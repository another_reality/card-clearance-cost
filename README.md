# Card Clearance Cost

.NetCore Implementation that show card clearance cost based on country. 

## Installation

Clone repository

```bash
git clone https://gitlab.com/another_reality/card-clearance-cost.git
```

On solution folder

```bash
dotnet build
```

Publish project

```bash
dotnet publish
```

Execute project

```bash
dotnet ClearanceCostApi/bin/Debug/netcoreapp2.2/ClearanceCostApi.dll -server.urls "http://127.0.0.1:5000"
```

Visit swagger at http://localhost:5000/index.html in order to play with the API

## Run tests

Unit and Integration test are implemented. Execute the following command on the solution folder.

```bash
dotnet test
```

## Database / Store

In this implementation not any database is used, instead BinList responses and calculation info are saved on project local in order to keep it simple.
If I would use a store mechanism, then I would definitely look at a nosql solution.

StoreFiles directories

```bash
# directory that hold the BinList responses. Title is the credit card BIN
StoreFile/CardInfo
```
```bash
# here you will find clearance cost by country. Used on calculation services
StoreFile/CardCost/CostsByCountry.json
```

## Environment

```bash
Environment=ASPNETCORE_ENVIRONMENT=Production
Environment=ASPNETCORE_URLS=http://127.0.0.1:5000
```

## TODO

 - Find a way to test 429 ToManyRequest error code returned by BinList. My main concern here is that BinList doesn't return this error code on exactly more than 10 requests by min.  

## License
[MIT](https://choosealicense.com/licenses/mit/)
